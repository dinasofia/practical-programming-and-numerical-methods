#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#define ode_stepper rkstep12

void ode_stepper(
        void f(int n,double x,double*y,double*dydx),
        int n, double x, double* y, double h, double* yh, double* dy, int max);

int ode_driver_a(void f(int n,double x,double*y,double*dydx),
	int n, double *ylist,double a,double b,double h,double acc,double eps, int max){
        // h er stepsize
  int i,k=0; double x,*y,s,err,normy,tol,yh[n],dy[n];

  while(a<b){
//  Vi tager de nye værdier der bliver puttet ind i x- og y-list.	Der er en i den 0'te indgang fra start af.
    x=a, y=ylist;

	if(x+h>b){h=b-x;}
	ode_stepper(f,n,x,y,h,yh,dy,max);
	s=0; for(i=0;i<n;i++){s+=dy[i]*dy[i]; err  =sqrt(s);}
  	s=0; for(i=0;i<n;i++){s+=yh[i]*yh[i]; normy=sqrt(s);}
    tol=(normy*eps+acc)*sqrt(h/(b-a));
    if(err<tol){
	a=x+h;
        //Her sørger vi for at vi ikke itererer flere gange end vi har sagt vi vil gennem max.
	for(i=0;i<n;i++){ylist[i]=yh[i];}// Opdater x og y med vores step.
      }
	if(err>0) {h*=pow(tol/err,0.25)*0.95;}
	else h*=2;
	k++;
// Her slutter vores while

	}
}


int ode_driver(void f(int n,double x,double*y,double*dydx),
	int n,double*xlist,double**ylist,double b,double h,double acc,double eps,int max){
	// h er stepsize
  int i,k=0; double x,*y,s,err,normy,tol,a=xlist[0],yh[n],dy[n];

  while(xlist[k]<b){
	// Vi tager de nye værdier der bliver puttet ind i x- og y-list. Der er en i den 0'te indgang fra start af.
    x=xlist[k], y=ylist[k]; if(x+h>b) h=b-x;

    ode_stepper(f,n,x,y,h,yh,dy,max);
    s=0; for(i=0;i<n;i++) s+=dy[i]*dy[i]; err  =sqrt(s);
    s=0; for(i=0;i<n;i++) s+=yh[i]*yh[i]; normy=sqrt(s);
    tol=(normy*eps+acc)*sqrt(h/(b-a));
    if(err<tol){ /* accept step and continue */
	k++;
	//Her sørger vi for at vi ikke itererer flere gange end vi har sagt vi vil gennem max.
	if(k>max-1) return -k;
      xlist[k]=x+h; for(i=0;i<n;i++)ylist[k][i]=yh[i];// Opdater x og y med vores step.
      }
    if(err>0) h*=pow(tol/err,0.25)*0.95; else h*=2;
    } /* Her slutter vores while */
  return k+1; } /* return the number of entries in xlist/ylist */

