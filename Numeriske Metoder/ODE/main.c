#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int ode_driver(
        void f(int n, double x, double*y, double*dydx),
        int n, double* xlist, double** ylist,
        double b, double h, double acc, double eps, int max);

int ode_driver_a(void f(int n,double x,double*y,double*dydx),
        int n, double *ylist,double a,double b,double h,double acc,double eps, int max);

void f(int n, double x, double* y, double* dydx){// Vores differentialligning
	dydx[0]=y[1];
	dydx[1]=-y[0];
	return;}
void f2(int n, double x, double* y, double* dydx){// Vores differentialligning
        dydx[0]=-10*y[0];
        dydx[1]=100*y[0];
        return;}

int main(){
        int n=2;
        int max=500;
        double *ylist = (double*)calloc(n,sizeof(double*));

        double pi=atan(1.)*4;   // Vi definerer pi
        double a=0, b=2*pi, h1=0.1, acc=0.01, eps=0.01; //start- og slutpunkter for vores funktion.

        ylist[0]=1, ylist[1]=0;
	double x;
	for(x=0.0;x<b;x+=0.3){
        int k = ode_driver_a(f,n,ylist,a,x,h1,acc,eps,max);
        if(k<0)printf("max steps reached in ode_driver\n");

        printf("%g %g %g %g %g\n",x,ylist[0],ylist[1],-sin(x),cos(x));
	a = x;
	}

        printf("\n\n");

/*
        double a1=0, b1=2*pi, h2 = 0.1; //start- og slutpunkter for vores funktion.

        double*x1list=(double*)calloc(max,sizeof(double));
        double**y1list=(double**)calloc(max,sizeof(double*));

        for(int i=0;i<max;i++) y1list[i]=(double*)calloc(n,sizeof(double)); // På hver indgang i ylist allokerer vi plads til 2 doubles

        x1list[0]=a1; y1list[0][0]=1; y1list[0][1]=0;
        int p = ode_driver(f,n,x1list,y1list,b1,h2,acc,eps,max);
        if(p<0)printf("max steps reached in ode_driver\n");
        for(int i=0;i<p;i++)printf("%g %g %g\n",x1list[i],y1list[i][0],cos(x1list[i]));





	printf("\n\n");
*/
        double a2=0, b2=0.5; //start- og slutpunkter for vores funktion.

        double*x2list=(double*)calloc(max,sizeof(double));
        double**y2list=(double**)calloc(max,sizeof(double*));

        for(int i=0;i<max;i++) y2list[i]=(double*)calloc(n,sizeof(double)); // På hver indgang i ylist allokerer vi plads til 2 doubles
	double h3 = 0.1;

        x2list[0]=a2; y2list[0][0]=1; y2list[0][1]=-10;
        int p = ode_driver(f2,n,x2list,y2list,b2,h3,acc,eps,max);
        if(p<0)printf("max steps reached in ode_driver\n");
        for(int i=0;i<p;i++)printf("%g %g %g\n",x2list[i],y2list[i][0],exp(-10*x2list[i]));


	return 0;

}
