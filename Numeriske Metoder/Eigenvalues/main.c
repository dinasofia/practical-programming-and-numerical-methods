#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define RND ((double)rand()/RAND_MAX)
#define FMT "%7.3f"

double jacobian(gsl_matrix *A, gsl_matrix *V);
void printm(gsl_matrix *A){

        for(int r=0;r<A->size1;r++){
                for(int c=0;c<A->size2;c++) printf(FMT,gsl_matrix_get(A,r,c));
                printf("\n");}
printf("\n\n");
}

int main(int argc, char** argv){
int n = 5;
if(argc>1) n=atoi(argv[1]);
gsl_matrix *A = gsl_matrix_calloc(n,n);
for(int i=0;i<n;i++)for(int j=i;j<n;j++){
	gsl_matrix_set(A,i,j,RND);
	double aij = gsl_matrix_get(A,i,j);
	gsl_matrix_set(A,j,i,aij);
}

gsl_matrix *Aog = gsl_matrix_calloc(n,n);
gsl_matrix_memcpy(Aog,A);
gsl_matrix *V = gsl_matrix_calloc(n,n);
gsl_matrix_set_identity(V);

jacobian(A,V);

if(n>9){printf("n=%i\n",n); return 0;}

gsl_matrix *C = gsl_matrix_calloc(n,n);
gsl_matrix *D = gsl_matrix_calloc(n,n);
gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,V,Aog,0.0,C);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,C,V,0.0,D);

printf("Symmetrisk matrix A: \n");
printm(Aog);
printf("Matricen V fra diagonalisering: \n");
printm(V);
printf("Matrix-produktet (V^T)AV, skal være lig med matricen D: \n");
printm(D);
printf("Matricen D, har egenværdier for A på diagonalen: \n");
printm(A);
gsl_matrix_free(D);
gsl_matrix_free(V);
gsl_matrix_free(A);
}
