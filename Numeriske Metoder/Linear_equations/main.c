#include <gsl/gsl_blas.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#define RND ((double)rand()/RAND_MAX)
#define FMT "%7.3f"

void qrdec(gsl_matrix*, gsl_matrix*);
void qrbak(gsl_matrix*,gsl_matrix*,gsl_vector*,gsl_vector*);
void inverse(gsl_matrix *A, gsl_matrix *B);

// Lille funktion der kan printe en matrix pænt ud i terminalen.
void printm(gsl_matrix *A){
	for(int r=0;r<A->size1;r++){
		for(int c=0;c<A->size2;c++) printf(FMT,gsl_matrix_get(A,r,c));
		printf("\n");}
}
//Og det samme for vektorer
void vector_print(gsl_vector *v){
        for(int i=0;i<v->size;i++) {printf(FMT,gsl_vector_get(v,i)); printf("\n");}
        printf("\n");
        }


int main(int argc, char** argv){
	size_t n=4,m=3;
	gsl_matrix *A = gsl_matrix_calloc(n,m);
	gsl_matrix *R = gsl_matrix_calloc(m,m);
	//Alle indgange i A sættes til et tilfældigt tal.
	for(int i=0;i<n;i++)for(int j=0;j<m;j++)gsl_matrix_set(A,i,j,RND);

	printf("\n");
	printf("---------------Opg. A.1---------------------\n");
	printf("QR dekomposition:\n");
	printf("Tilfældig %lux%lu matrix A:\n",n,m); printm(A);
	qrdec(A,R);

	printf("\n");
	printf("A opløses i dekompositionen A=QR\n");
	printf("Matricen Q:\n"); printm(A);
	printf("\n");
	printf("Matricen R:\n"); printm(R);
	printf("R skal gerne være øvre triangulær, hvilket den her er.\n\n");

	// Vi laver matrixproduktet (Q^T)Q, der skal give identitetsmatricen
	gsl_matrix *qtq = gsl_matrix_calloc(m,m);
	gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,A,A,0.0,qtq);

	printf("Matrix-produktet (Q^T)Q:\n"); printm(qtq);
	printf("(Q^T)Q skal gerne være I, hvilket vi ser den bliver.\n\n");

	// For at tjekke at produktet QR virkelig er lig A beregner vi det her
	gsl_matrix *qr = gsl_matrix_calloc(n,m);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,A,R,0.0,qr);
	printf("Matrix-produktet QR:\n"); printm(qr);
        printf("QR skal gerne være A, hvilket vi ser den bliver.\n\n");

        printf("---------------Opg. A.2---------------------\n");
	printf("Når vi nu kan lave dekompositionen, vil vi \nprøve at bruge den til at løse et \nlineært lignings-system: \n\n");
	m=3;
	// Vi laver en tilfældig kvadratisk matrix A
	gsl_matrix *A_ny = gsl_matrix_calloc(m,m);
	for(int i=0;i<m;i++)for(int j=0;j<m;j++)gsl_matrix_set(A_ny,i,j,RND);

	printf("En tilfældig %lux%lu matrix A:\n",m,m); printm(A_ny); printf("\n");
	// Vi laver en matrix Q_ny, som er den vi får ud af dekompositionen, vi kopierer A over i den, så vi kan beholde A
	gsl_matrix *Q_ny = gsl_matrix_alloc(m,m);
	gsl_matrix_memcpy(Q_ny,A_ny);
	// Vi laver en tilfældig vektor, der er højresiden i vores ligningssystem.
	gsl_vector *b = gsl_vector_alloc(m);
	for(int i=0;i<m;i++)gsl_vector_set(b,i,RND);

	printf("En tilfældig vektor b, der er høresiden i vores lign. system:\n");
	vector_print(b);
	// Der allokeres en matrix R, og der laves dek. af Q_ny, der jo indeholder matricen A
	gsl_matrix *R_ny = gsl_matrix_calloc(m,m);
	qrdec(Q_ny,R_ny);

	// Der laver back substitution, og vi får løsningen x ud.
	gsl_vector *x = gsl_vector_alloc(m);
	qrbak(Q_ny,R_ny,b,x);
	printf("Vektoren x der er løsningen på lign. systemet:\n"); vector_print(x);

	//Her beregner vi produktet Ax, for at se at den fundne løsning er korrekt
	gsl_blas_dgemv(CblasNoTrans,1.0,A_ny,x,0.0,b);
	printf("Produktet Ax, skal være lig b:\n"); vector_print(b);

        printf("---------------Opg. B---------------------\n");
	printf("Vi bruger den samme matrix A, som i opg. A.2:\n");
	printm(A_ny); printf("\n");

	//Den inverse til A beregnes ved dek. og back subs.
	gsl_matrix *invA = gsl_matrix_calloc(m,m);
	gsl_matrix_memcpy(Q_ny,A_ny);
	inverse(Q_ny,invA);
	printf("Den inverse matrix til A: A^{-1}:\n"); printm(invA); printf("\n");
	// Produktet A*invA beregnes
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,A_ny,invA,0.0,R_ny);
	printf("Matrixproduktet AA^-1, skal være lig I:\n"); printm(R_ny); printf("\n");
	//Produktet invA*A beregnes
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,invA,A_ny,0.0,R_ny);
	printf("Matrixproduktet (A^-1)A, skal være lig I:\n"); printm(R_ny); printf("\n");

}
