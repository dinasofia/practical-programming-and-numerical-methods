#include<gsl/gsl_vector.h>
#ifndef HAVE_NEURONS
#define HAVE_NEURONS
typedef struct {
	int n; //antallet af neurons i det skjulte lag
	double(*f)(double); 	/* Vores activation function, den som de skjulte neuroner bruger.
 Vi kunne specificere den her, men på denne måde kan vi prøve forskellige funktioner af.*/
	gsl_vector* data;
	} neurons;	// Denne struct indeholder nu al den information vores netværk har brug for
neurons* neurons_alloc(int n, double(*f)(double));	//Denne funktion allokerer et nyt netværk, og laver en pointer til det
void neurons_free(neurons* nw);
double neurons_feed_forward(neurons* nw, double x);	// Tager nw og inputtet x, og returnerer outputtet y
void neurons_train(neurons* nw, gsl_vector* xlist, gsl_vector* ylist); //
neurons* neurons_alloc_2D(int n, double(*f)(double));
double neurons_feed_forward_2D(neurons* nw, double x, double y);
void neurons_train_2D(neurons* nw, gsl_vector* vx, gsl_vector* vy, gsl_vector* vf);

#endif

