#include<gsl/gsl_vector.h>
#include<stdio.h>
#include<math.h>
#include"neurons.h"
#include<gsl/gsl_multimin.h>
//1D
neurons* neurons_alloc(int n, double(*f)(double)){
	neurons* nw = malloc(sizeof(neurons));	// Her allokerer vi hukommelse til vores struct
	nw->n=n;
	nw->f=f;	// Holder en pointer til vores funktion.
	nw->data = gsl_vector_alloc(3*n);	// Her skal der være plads til vores a'er, b'er og w'er
	return nw;
}
void neurons_free(neurons* nw){
        gsl_vector_free(nw->data);
        free(nw);
}
//2D
neurons* neurons_alloc_2D(int n, double(*f)(double)){
        neurons* nw_2D = malloc(sizeof(neurons));  // Her allokerer vi hukommelse til vores struct
        nw_2D->n=n;
        nw_2D->f=f;        // Holder en pointer til vores funktion.
        nw_2D->data = gsl_vector_alloc(5*n);       // Her skal der være plads til vores a'er, b'er og w'er
        return nw_2D;
}
//1D
double neurons_feed_forward(neurons* nw, double x){
	double s = 0;	// Vi laver den sum der skal være vores output
	for(int i=0; i< nw->n; i++ ){
		double a=gsl_vector_get(nw->data,0*nw->n+i);
		double b=gsl_vector_get(nw->data,1*nw->n+i);
		double w=gsl_vector_get(nw->data,2*nw->n+i);
		s+=nw->f((x+a)*b)*w;
	}
	return s;
}


//1D
void neurons_train(neurons* nw, gsl_vector* vx, gsl_vector* vf){
	double delta(const gsl_vector* p,void*params){	//Det er vores mismatch funktion. P er vores parametre.
		gsl_vector_memcpy(nw->data,p);// Vi sætter vores netværk til at indeholde vores parametre.
		double s = 0;	// Vi laver her den funktion vi gerne vil minimere.
		for(int i=0;i< vx->size;i++){
			double x=gsl_vector_get(vx,i);
			double f=gsl_vector_get(vf,i);
			double y=neurons_feed_forward(nw,x);
			s+=fabs(y-f);	//Vi laver her en sum over afvigelsen mellem den 'rigtige' funktionsværdi, og det vores netværk finder
		}
		return s/vx->size;
	}
	gsl_vector* p=gsl_vector_alloc(nw->data->size);
	gsl_vector* h=gsl_vector_alloc(nw->data->size);
	gsl_vector_set_all(h,0.2);
	gsl_multimin_function F={.f=delta,.n=nw->data->size,.params=NULL};
	gsl_vector_memcpy(p,nw->data);	//Vi giver vores parametre tilbage til vores netværk, som vores nye startgæt

	gsl_multimin_fminimizer* mini = gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2,p->size);
	gsl_multimin_fminimizer_set(mini,&F,p,h);
	int iter=0;
	do{
		iter++;
		gsl_multimin_fminimizer_iterate(mini);
		if(mini->size<1e-4)break;
	}while(iter<1000);
	//minimizer skal vi finde det sæt af p'er der minimerer delta
	gsl_vector_memcpy(nw->data,mini->x);
	gsl_vector_free(p);
	gsl_vector_free(h);
	gsl_multimin_fminimizer_free(mini);
}

//2D
double neurons_feed_forward_2D(neurons* nw, double x, double y){
        double s = 0;   // Vi laver den sum der skal være vores output
	for(int i=0; i< nw->n; i++ ){
                double ax=gsl_vector_get(nw->data,5*i+0);
                double bx=gsl_vector_get(nw->data,5*i+1);
                double ay=gsl_vector_get(nw->data,5*i+2);
                double by=gsl_vector_get(nw->data,5*i+3);
	        double w=gsl_vector_get(nw->data,5*i+4);
                s+=nw->f((x+ax)/bx)* nw->f((y+ay)/by)*w;
        }
        return s;
}


void neurons_train_2D(neurons* nw, gsl_vector* vx, gsl_vector* vy, gsl_vector* vf){
        double delta(const gsl_vector* p,void*params){  //Det er vores mismatch funktion. P er vores parametre.
                gsl_vector_memcpy(nw->data,p);// Vi sætter vores netværk til at indeholde vores parametre.
                double s = 0;   // Vi laver her den funktion vi gerne vil minimere.
                for(int i=0;i< vx->size;i++){
                        double x=gsl_vector_get(vx,i);
			double y=gsl_vector_get(vy,i);
                        double f=gsl_vector_get(vf,i);
                        double q=neurons_feed_forward_2D(nw,x,y);
                        s+=fabs(q-f);   //Vi laver her en sum over afvigelsen mellem den 'rigtige' funktionsværdi, og det vores netværk finder
                }
                return s/vx->size;
        }
        gsl_vector* p=gsl_vector_alloc(nw->data->size);
        gsl_vector* h=gsl_vector_alloc(nw->data->size);
        gsl_vector_set_all(h,0.1);
        gsl_multimin_function F={.f=delta,.n=nw->data->size,.params=NULL};
        gsl_vector_memcpy(p,nw->data);  //Vi giver vores parametre tilbage til vores netværk, som vores nye startgæt

        gsl_multimin_fminimizer* mini = gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2,p->size);
        gsl_multimin_fminimizer_set(mini,&F,p,h);
        int iter=0;
        do{
                iter++;
                gsl_multimin_fminimizer_iterate(mini);
                if(mini->size<1e-2)break;
        }while(iter<1000);
        //minimizer skal vi finde det sæt af p'er der minimerer delta
        gsl_vector_memcpy(nw->data,mini->x);
        gsl_vector_free(p);
        gsl_vector_free(h);
        gsl_multimin_fminimizer_free(mini);
}

