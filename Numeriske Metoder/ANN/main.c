#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<math.h>
#include"neurons.h"
#define RND (double)rand()/RAND_MAX

double activation_function(double x){return x*exp(-x*x);}
//double activation_function(double x){return atan(x);}
double function_to_fit(double x){return sin(x)*sin(x)+cos(x);}
double dim2_function(double x, double y){return exp(-x*x-y*y);}
int main(){
	int n=7;

	neurons* nw=neurons_alloc(n,activation_function);
	double a=-3,b=3;	// endepunkter for 1D
	int nx_1D = 20;	//antallet af x-punkter i 1D
	gsl_vector* x_vek = gsl_vector_alloc(nx_1D); // input vektor for 1D
	gsl_vector* vf = gsl_vector_alloc(nx_1D); // output vektor for 1D


	double ax=-1, bx=1, ay=-1, by=1; // endepunkter for 2D
	int nx=20, ny=20; //antallet af x- og y-punkter i 1D
	gsl_vector* vx=gsl_vector_alloc(nx*ny); // input vektor for x i 2D tilfældet
        gsl_vector* vy=gsl_vector_alloc(ny*nx); // input vektor for y i 2D tilfældet
        gsl_vector* vf_2D=gsl_vector_alloc(nx*ny); // output matrix i 2D tilfældet

	// Her laver vi en vektor med funktionen evalueret i en masse punkter spredt ud over intervallet.
	// for 1D tilfældet
	for(int i=0;i<nx_1D;i++){
		double x=a+(b-a)*i/(nx_1D-1);
		double fx=function_to_fit(x);
		gsl_vector_set(x_vek,i,x);
		gsl_vector_set(vf,i,fx);
	}
	for(int i=0;i < nw->n;i++){
		double k=1.1;
		gsl_vector_set(nw->data,0+3*i,a*k+(b-a)*k*i/(nw->n-1));
                gsl_vector_set(nw->data,1+3*i,1);
                gsl_vector_set(nw->data,2+3*i,1);
	}
	neurons_train(nw,x_vek,vf); //Her træner vi så vores netværk med vores data for 1D
	for(int i=0;i < nw->n;i++){
		double a=gsl_vector_get(nw->data,0+3*i);
                double b=gsl_vector_get(nw->data,1+3*i);
                double w=gsl_vector_get(nw->data,2+3*i);
		fprintf(stderr,"i=%i, a,b,w=%g %g %g\n",i,a,b,w);
	}

        for(int i=0;i< x_vek->size;i++){
                double x=gsl_vector_get(x_vek,i);
                double fx=gsl_vector_get(vf,i);
                printf("%g %g\n",x,fx);
        }
        printf("\n\n");

        double dz=1.0/64;
        for(double z=a;z<=b;z+=dz){
                double y=neurons_feed_forward(nw,z);
                printf("%g %g\n",z,y);
        }


        // Her laver vi en vektor med funktionen evalueret i en masse punkter spredt ud over intervallet.
        // for 2D tilfældet
	neurons* nw_2D=neurons_alloc_2D(n,activation_function);
        for(int i=0;i<nx;i++)for(int j=0;j<nx;j++){
                double x=ax+(bx-ax)*i/(nx-1);
		double y=ay+(by-ay)*j/(ny-1);
                double fx=dim2_function(x,y);
		int k = i*nx+j;

                gsl_vector_set(vx,k,x);
		gsl_vector_set(vy,k,y);
                gsl_vector_set(vf_2D,k,fx);
        }
        for(int i=0;i < nw_2D->n;i++){
                gsl_vector_set(nw_2D->data,0+5*i,ax+(bx-ax)*i/(nw_2D->n-1));//Her sætter vi vores ax
                gsl_vector_set(nw_2D->data,1+5*i,1); // bx
                gsl_vector_set(nw_2D->data,2+5*i,ay+(by-ay)*i/(nw_2D->n-1));//ay
		gsl_vector_set(nw_2D->data,3+5*i,1); //by
		gsl_vector_set(nw_2D->data,4+5*i,1); // Og vores vægt, som der den samme uafh. af x og y
        }
        neurons_train_2D(nw_2D,vx,vy,vf_2D); //Her træner vi så vores netværk med vores data for 1D

	printf("\n\n");

	for(int i=0;i< vx->size;i++){
		double x=gsl_vector_get(vx,i);
		double y=gsl_vector_get(vy,i);
		double fx=gsl_vector_get(vf_2D,i);
		printf("%g %g %g\n",x,y,fx);
	}
	printf("\n\n");

	double dz_2D=1.0/20;
	for(double z=ax;z<=bx;z+=dz_2D)for(double v=ay;v<=by;v+=dz_2D){
		double q=neurons_feed_forward_2D(nw_2D,z,v);
		printf("%g %g %g\n",z,v,q);
	}

gsl_vector_free(x_vek);
gsl_vector_free(vf);
gsl_vector_free(vx);
gsl_vector_free(vy);
gsl_vector_free(vf_2D);
neurons_free(nw);
neurons_free(nw_2D);
return 0;
}

