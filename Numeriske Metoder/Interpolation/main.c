#include<stdio.h>
#include<stdlib.h>
#define RND (double)rand()/RAND_MAX
#include"qspline.h"
#include"cspline.h"

double linear_interp(int, double*, double*, double);
double lin_integral(int, double*, double*,double);

int main(){
	int n= 10;
	double x[n], y[n];
	for(int i=0; i<n; i++){
		x[i]=0.5+i;
		y[i]=0.2*i+RND;
		printf("%g %g \n",x[i],y[i]);
	}
	printf("\n\n");
	qspline* Q = qspline_alloc(n,x,y);
	cubic_spline* C = cubic_spline_alloc(n,x,y);
	double dz = 0.1;
	for(double z=x[0];z<=x[n-1];z+=dz){
		double linz = linear_interp(n,x,y,z);
		double quadz = qspline_eval(Q,z);
		double cubez = cubic_spline_eval(C,z);
		double l_integral = lin_integral(n,x,y,z);
		double q_integral = qspline_integral(Q,z);
		double c_integral = cubic_spline_integral(C,z);
		double q_deriv = qspline_derivative(Q,z);
		double c_deriv = cubic_spline_derivative(C,z);
		printf("%g %g %g %g %g %g %g %g %g\n",z, linz, quadz, cubez, l_integral, q_integral, c_integral, q_deriv, c_deriv);
	}
qspline_free(Q);
return 0;
}
