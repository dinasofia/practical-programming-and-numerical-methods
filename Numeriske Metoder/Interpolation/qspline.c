#include<stdlib.h>
#include<stdio.h>
#include<assert.h>
#include<math.h>
#include "qspline.h"

qspline* qspline_alloc(int n, double* x, double* y){
	qspline* s = malloc(sizeof(qspline));
	s->b = malloc((n-1)*sizeof(double));
	s->c = malloc((n-1)*sizeof(double));
	s->x = malloc(n*sizeof(double));
	s->y = malloc(n*sizeof(double));
	s->n = n;
	int i;
	for(i=0;i<n;i++){
		s->x[i]=x[i];
		s->y[i]=y[i];
	}
	double p[n-1], h[n-1];
	for(i=0;i<n-1;i++){
		h[i]=x[i+1]-x[i];	// Her laver vi en delta x
		p[i]=(y[i+1]-y[i])/h[i];
	}
	s->c[0]=3;		// Første c defineres arbitrært
	for(i=0;i<n-2;i++)	// Så laver vi de andre c'er ved rekursion.
		s->c[i+1]=(p[i+1]-p[i]-s->c[i]*h[i])/h[i+1];
	for(i=0;i<n-1;i++)
		s->b[i]=p[i]-s->c[i]*h[i];
	return s;
}

double qspline_eval(qspline *s, double z){
	assert(z>=s->x[0] && z<=s->x[s->n-1]);	// z skal ligge i intervallet mellem den højeste og laveste z
	int i=0, j=s->n-1;
	while(j-i>1){
		int m=(i+j)/2;
		if(z>s->x[m]) i=m;
		else j=m;
	}
	double h=z-s->x[i];
	return s->y[i]+h*(s->b[i]+h*s->c[i]);
}

double qspline_derivative(qspline *s, double z){
	assert(z>=s->x[0] && z<=s->x[s->n-1]);
	int i=0, j=s->n-1;
	while(j-i>1){
		int m=(i+j)/2;
		if(z>s->x[m]) i=m;
		else j=m;
		}
	double h=z-s->x[i];
	return s->b[i]+2*s->c[i]*h;
}

double qspline_integral(qspline *s, double z){
	assert(z>=s->x[0] && z<=s->x[s->n-1]);
	int i=0, j=s->n-1;
	while(j-i>1){
		int m=(i+j)/2;
		if(z>s->x[m]) i=m;
		else j=m;
		}
	double sum=0;
	for(int k=0; k<i; k++){
		double hk=s->x[k+1]-s->x[k];
		sum += hk*s ->y[k]+0.5*hk*hk*s->b[k]+(1./3)*hk*hk*hk*s->c[k];
		}
	double h=z-s->x[i];
	sum += h*s ->y[i]+0.5*h*h*s->b[i]+(1./3)*h*h*h*s->c[i];

	return sum;
}


void qspline_free(qspline *s){
	free(s->x); free(s->y); free(s->b); free(s->c); free(s);
}

