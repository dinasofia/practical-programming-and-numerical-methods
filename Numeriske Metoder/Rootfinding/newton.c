#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"qr.h"

void newton(void f(gsl_vector* x,gsl_vector* fx), gsl_vector* x, double dx, double eps){
	int n=x->size;
	gsl_matrix* J = gsl_matrix_alloc(n,n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_vector* fx = gsl_vector_alloc(n);
	gsl_vector* z  = gsl_vector_alloc(n);
	gsl_vector* fz = gsl_vector_alloc(n);
	gsl_vector* df = gsl_vector_alloc(n);
	gsl_vector* Dx = gsl_vector_alloc(n);
	while(1){
		f(x,fx);
		for (int j=0;j<n;j++){
			gsl_vector_set(x,j,gsl_vector_get(x,j)+dx);//Laeg et lille skridt til x
			f(x,df);// lad df vaere funktionen evalueret i x+dx
			gsl_vector_sub(df,fx); /* df=f(x+dx)-f(x) */
			for(int i=0;i<n;i++) gsl_matrix_set(J,i,j,gsl_vector_get(df,i)/dx); /*Her definerer vi vores jacobian matrix*/
			gsl_vector_set(x,j,gsl_vector_get(x,j)-dx);	// Her traekker vi vores lille skridt i x og y tilbage igen.
			}
		qrdec(J,R);	// Her laver vi en QR-dek af vores J. J laves om til 'Q' og R fyldes med en rektangulaer matrix
		qrsolve(J,R,fx,Dx);	// Her løses lign. sys Dx = R^(-1)J^(T)fx
		gsl_vector_scale(Dx,-1); // Vi ganger vektoren Dx med -1
		double s=1;
		while(1){
			gsl_vector_memcpy(z,x); // Her kopierer vi vektoren x over i vektoren z.
			gsl_vector_add(z,Dx);	// Laeg de to vektorer sammen: z = z+Dx
			f(z,fz);		// Den afledte af Rosenbrock evalueres i z, og resultatet gemmes i fz
			if( gsl_blas_dnrm2(fz)<(1-s/2)*gsl_blas_dnrm2(fx) || s<0.02 ) break; /*Saa her svarer s til det der hedder lamda i
'bogen'. Så nu tjekker vi om vores funktionsvaerdi i z, er mindre end (1-s/2)*fx. Hvis den er det saa tager vi et step. Hvis den ikke er saa
tjekker vi om vores s en mindre end den mindste tilladte vaerdi. Hvis den er det saa breaker vi, ellers saa goer vi s mindre og proever igen. */
			s*=0.5;
			gsl_vector_scale(Dx,0.5);
			}
		gsl_vector_memcpy(x,z);		// Efter taget step, smid ny x,y-vaerdi fra z over i x
		gsl_vector_memcpy(fx,fz);	//Og nye funktionsvaerdier over i fx
		if( gsl_blas_dnrm2(Dx)<dx || gsl_blas_dnrm2(fx)<eps ) break;
		}
	gsl_matrix_free(J);
	gsl_matrix_free(R);
	gsl_vector_free(fx);
	gsl_vector_free(z);
	gsl_vector_free(fz);
	gsl_vector_free(df);
	gsl_vector_free(Dx);
}
/*
def newton_jacobian(f:"function", jacobian:"jacobian", x_start:vector, eps:float=1e-3):
	x=x_start.copy()
	while True :
		fx=f(x)
		J=jacobian(x)
		givens.qr(J)
		Dx = givens.solve(J,-fx)
		s=2
		while True :
			s/=2
			y=x+Dx*s
			fy=f(y)
			if fy.norm()<(1-s/2)*fx.norm() or s<0.02 : break
		x=y; fx=fy;
		if fx.norm()<eps : break
	return x;
*/
