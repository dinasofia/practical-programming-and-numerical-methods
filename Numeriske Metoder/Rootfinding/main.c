#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<stdio.h>
#include<math.h>

int newton (
	void f(gsl_vector* x,gsl_vector* fx),
	gsl_vector* x, double dx, double eps);

void vector_print(char* s,gsl_vector* v){
	printf("%s",s);
	for(int i=0;i<v->size;i++)printf("%10.4g ",gsl_vector_get(v,i));
	printf("\n");
	}

int main() {
printf("-------------Root finding A---------------\n");
	int calls = 0;
	gsl_vector* x=gsl_vector_alloc(2);
	gsl_vector* fx=gsl_vector_alloc(2);

        void f_lign_sys(gsl_vector* p,gsl_vector* fx){
		double A = 10000.0;
                calls++;
                double x=gsl_vector_get(p,0), y=gsl_vector_get(p,1);
                /*Her saettes de to indgange i fx til de to funktioner vi skal løse*/
                gsl_vector_set(fx,0, A*x*y-1);
                gsl_vector_set(fx,1, exp(-x)+exp(-y)-1-1./A);
                }
// Vi giver fitteren nogle startgæt
printf("------------ Løse ligningssystem --------------\n");
        gsl_vector_set(x,0,-2);
        gsl_vector_set(x,1,8);
        printf("Her finder vi løsningen til ligningssystemet:\n A*x*y = 1 \n exp(-x)+exp(-y) = 1+1/A \n med A=10000\n\n");
	printf("Det svarer til at finde rødderne til: \n A*x*y-1 = 0 \n exp(-x)+exp(-y)-1-1/A = 0 \n\n");
        vector_print("       Startgæt på x og y: ",x);
        f_lign_sys(x,fx);
        vector_print("Fkt. værdi i startgættene: ",fx);
        newton(f_lign_sys,x,1e-6,1e-3);

	printf("\n");
        printf("Antallet af kald til funktionen under løsningen: \ncalls = %i\n",calls);
        printf("\n");

        vector_print("x- og y-værdier der er løsninger til ligningssystemet: ",x);
        f_lign_sys(x,fx);
        vector_print("                             Fkt. værdi ved løsningen: ",fx);
        printf("\n");


	calls = 0;
	void f_rosen(gsl_vector* p,gsl_vector* fx){
		calls++;
		double x=gsl_vector_get(p,0), y=gsl_vector_get(p,1);
		/*Her saettes de to indgange i fx til den afledte af
		rosenbrock fkt. mht. hhv x og y*/
		gsl_vector_set(fx,0, 2*(1-x)*(-1)+100*2*(y-x*x)*(-1)*2*x);
		gsl_vector_set(fx,1, 100*2*(y-x*x));
		}
printf("------------ Rosenbrock funktion --------------\n");
	gsl_vector_set(x,0,-2);
	gsl_vector_set(x,1,8);
	printf("Find ekstremum for Rosenbrock-funktionen:\n f(x,y)=(1-x)^2 + 100*(y-x^2)^2\n\n");
	printf("Gøres ved at finde rødderne for de afledte:\n dfdx = -2*(1-x)-400*(y-x^2)*x\n dfdy = 200*(y-x^2)\n\n");
        vector_print("       Startgæt på x og y: ",x);
	f_rosen(x,fx);
        vector_print("Fkt. værdi i startgættene: ",fx);
	newton(f_rosen,x,1e-6,1e-3);

        printf("\n");
        printf("Antallet af kald til funktionen under løsningen: \ncalls = %i\n",calls);
        printf("\n");

        vector_print("x- og y-værdier der er løsninger til ligningssystemet: ",x);
        f_rosen(x,fx);
        vector_print("                             Fkt. værdi ved løsningen: ",fx);
        printf("\n");


	calls = 0;
        void f_himmel(gsl_vector* p,gsl_vector* fx){
                calls++;
                double x=gsl_vector_get(p,0), y=gsl_vector_get(p,1);
                /*Her saettes de to indgange i fx til den afledte af
                Himmelblau fkt. mht. hhv x og y*/
                gsl_vector_set(fx,0, 2*(x*x+y-11)*2*x+2*(x+y*y-7));
                gsl_vector_set(fx,1, 2*(x*x+y-11)+2*(x+y*y-7)*2*y);
                }

	printf("\n");

        gsl_vector_set(x,0,-2);
        gsl_vector_set(x,1,8);
printf("------------ Himmelblau funktion --------------\n");
        printf("Find ekstremum for Himmmelblau-funktionen:\n f(x,y)= (x^2+y-11)^2+ (x+y^2-7)^2 \n\n");
        printf("Gøres ved at finde rødderne for de afledte:\n dfdx = 4*x*(x^2+y-11)+2*(x+y^2-7)\n dfdy = 2*(x^2+y-11)+4*y*(x+y^2-7)\n\n");
        vector_print("       Startgæt på x og y: ",x);
        f_himmel(x,fx);
        vector_print("Fkt. værdi i startgættene: ",fx);
        newton(f_himmel,x,1e-6,1e-3);

        printf("\n");
        printf("Antallet af kald til funktionen under løsningen: \ncalls = %i\n",calls);
        printf("\n");

        vector_print("x- og y-værdier der er løsninger til ligningssystemet: ",x);
        f_himmel(x,fx);
        vector_print("                             Fkt. værdi ved løsningen: ",fx);
        printf("\n");


}
