#define pi 3.14159265358979323846264338327950288419716939937510
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_rng.h>

void plainmc(int dim, double a[], double b[], double f(double* x),
	int N, double* result, double* error);

double fun(double x[]){return x[0]+x[1]*x[2];}

double fun2(double x[]){return cos(x[0])+exp(x[1])*x[2];}

double fun3(double x[]){return (1/pow(M_PI,3.))*pow(1-cos(x[0])*cos(x[1])*cos(x[2]),-1.);}

int main(int argc, char **argv){
#pragma omp parallel sections
{
	#pragma omp section
{
	int N, d=3;
	double exact=(pow(M_PI,4)/2)*(1+M_PI/2);
	double a[] ={0,0,0}; double b[]={pi,pi,pi};
	double result, error;
	N=(1000000);

	plainmc(d,a,b,&fun,N,&result,&error);

	printf("\nAll integrals are taken with limits [0;pi] on all three variables\n\n");
	printf("-------------- Integral of x+y*z: -------------\n");
	printf("N\t= %d\n",N);
	printf("result\t= %g\n",result);
	printf("exact\t= %g\n",exact);
	printf("estimated error\t= %g\n",error);
	printf("actual error\t= %g\n",fabs(result-exact));}

#pragma omp section
{
        int N2, d=3;
        double exact2=(pow(pi,3)/2)*(pow(M_E,pi)-1);
        double a[] ={0,0,0}; double b[]={pi,pi,pi};
        double result2, error2;
        N2=(1e6);

        plainmc(d,a,b,&fun2,N2,&result2,&error2);
        printf("\n-------------- Integral of cos(x)+exp(y)*z: -------------\n");
        printf("N\t= %d\n",N2);
        printf("result\t= %g\n",result2);
        printf("exact\t= %g\n",exact2);
        printf("estimated error\t= %g\n",error2);
        printf("actual error\t= %g\n",fabs(result2-exact2));}

#pragma omp section
{
        int N3, d=3;
        double exact3=1.39320393;
        double a[] ={0,0,0}; double b[]={pi,pi,pi};
        double result3, error3;
        N3=(1e7);

        plainmc(d,a,b,&fun3,N3,&result3,&error3);
        printf("\n----- Integral of (1-cos(x)*cos(y)*cos(z))^-1: -------------\n");
        printf("N\t= %d\n",N3);
        printf("result\t= %g\n",result3);
        printf("exact\t= %g\n",exact3);
        printf("estimated error\t= %g\n",error3);
        printf("actual error\t= %g\n",fabs(result3-exact3));


	printf("\n\n");}

#pragma omp section
{
	int i, N, d=3;
        double a[] ={0,0,0}; double b[]={pi,pi,pi};
	for(i=1;i<100;i+=5){
        double result, error;
        N=i*1.5e4;
	double err = 1/sqrt(N);
        plainmc(d,a,b,&fun,N,&result,&error);
        printf("%d %g %g\n",N, error,err);
	}
}}
return 0;
}

