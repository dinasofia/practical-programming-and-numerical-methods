#include<math.h>
#include<assert.h>
#include<stdio.h>
double adapt
	(double f(double),double a,double b,double acc,double eps);

int main()
{
printf("---------------- Adaptive Integration, A ------------------\n");
	int calls=0;
	double a=0,b=1,acc=1e-4,eps=1e-4;	// Definer grænser på integrationsintervallet, og de to præcisioner.
	double s(double x){calls++; return sqrt(x);}; //nested function
	calls=0;
	double Q=adapt(s,a,b,acc,eps);
	double exact=2./3;
	printf("Integrating sqrt(x) from %g to %g\n",a,b);
	printf("acc=%g eps=%g\n",acc,eps);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));

	calls=0;
	a=0,b=1,acc=1e-4,eps=1e-4;
	double f(double x){calls++; return 1/sqrt(x);}; //nested function
	calls=0;
	Q=adapt(f,a,b,acc,eps);
	exact=2;
	printf("\nIntegrating 1/sqrt(x) from %g to %g\n",a,b);
	printf("acc=%g eps=%g\n",acc,eps);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));

	a=0,b=1,acc=0.001,eps=0.001;
	double f2(double x){calls++; return log(x)/sqrt(x);}; //nested function
	calls=0;
	Q=adapt(f2,a,b,acc,eps);
	exact=-4;
	printf("\nIntegrating log(x)/sqrt(x) from %g to %g\n",a,b);
	printf("acc=%g eps=%g\n",acc,eps);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));

        a=0,b=1,acc=1e-7,eps=1e-7;
        double fancy_func(double x){calls++; return 4*sqrt(1-pow(1-x,2.));}; //nested function
        calls=0;
        Q=adapt(fancy_func,a,b,acc,eps);
        exact=M_PI;
        printf("\nIntegrating 4*sqrt(1-(1-x)^2) from %g to %g\n",a,b);
        printf("acc=%g eps=%g\n",acc,eps);
        printf("              Q = %lg\n",Q);
        printf("          exact = %g\n",exact);
        printf("          calls = %d\n",calls);
        printf("estimated error = %g\n",acc+fabs(Q)*eps);
        printf("   actual error = %g\n",fabs(Q-exact));

return 0 ;
}
