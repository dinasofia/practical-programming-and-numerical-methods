#include<math.h>
#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include"qr.h"

void lsfit(
        int m, double f(int i,double x),
        gsl_vector* x, gsl_vector* y, gsl_vector* dy,
        gsl_vector* c, gsl_matrix* S);

int main(){
	double x[] = {0.1,1.33,2.55,3.78,5,6.22,7.45,8.68,9.9};
	double y[] = {-15.3,0.32,2.45,2.75,2.27,1.35,0.157,-1.23,-2.75};
	double dy[] = {1.04,0.594,0.983,0.998,1.11,0.398,0.535,0.968,0.478};
	int n=sizeof(x)/sizeof(x[0]);

	for(int i=0;i<n;i++)printf("%g %g %g\n",x[i],y[i],dy[i]);
	printf("\n\n");

	gsl_vector* vx = gsl_vector_alloc(n);
	gsl_vector* vy = gsl_vector_alloc(n);
	gsl_vector* vdy = gsl_vector_alloc(n);
	for(int i=0;i<n;i++){ /* Lav vektorer der indeholder vores givne 'data'*/
		gsl_vector_set(vx,i,x[i]);
		gsl_vector_set(vy,i,y[i]);
		gsl_vector_set(vdy,i,dy[i]);
		}

	int m=3;
	double funs(int i, double x){
   		switch(i){
   			case 0: return log(x); break;
   			case 1: return 1;   break;
   			case 2: return x;     break;
   			default: return NAN;
   			}
		}

	gsl_vector* c = gsl_vector_alloc(m);/*Lav vektoren c, som er løsningen
		til vores lign. sys*/
	gsl_matrix* S = gsl_matrix_alloc(m,m);
	lsfit(m,funs,vx,vy,vdy,c,S);

	gsl_vector* dc = gsl_vector_alloc(m);
	for(int k=0;k<m;k++){
		double skk=gsl_matrix_get(S,k,k);
		gsl_vector_set(dc,k,sqrt(skk));
		}

	double fit(double x){
		double s=0;
		for(int k=0;k<m;k++)s+=gsl_vector_get(c,k)*funs(k,x);
		return s;
		}

	double fit_plus(int i, double x){
		return fit(x)+gsl_vector_get(dc,i)*funs(i,x);
		}

	double fit_minus(int i, double x){
		return fit(x)-gsl_vector_get(dc,i)*funs(i,x);
		}

	double z,dz=(x[n-1]-x[0])/90;
	for(int i=0;i<m;i++){
	z=x[0]-dz/2;
		do{
		printf("%g %g %g %g\n",z,fit(z),fit_plus(i,z),fit_minus(i,z));
		z+=dz;
		}while(z<x[n-1]+dz);
	printf("\n\n");
	}


return 0;
}
