#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"qr.h"
#include<math.h>

int newton(double f(gsl_vector* x,gsl_vector* df, gsl_matrix* H), gsl_vector* xstart, double eps){
	int n=xstart->size, steps = 0;
	double alpha = 1e-4, lambda_min = 1.0/64;
	gsl_vector* x = xstart;
	gsl_vector* x_step = gsl_vector_alloc(n);

	gsl_vector* df = gsl_vector_alloc(n);
	gsl_matrix* H = gsl_matrix_alloc(n,n);
	gsl_matrix* Q = gsl_matrix_alloc(n,n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_matrix* invH = gsl_matrix_alloc(n,n);
	gsl_vector* delta_x = gsl_vector_alloc(n);

	do{
		f(x,df,H);
		gsl_matrix_memcpy(Q,H);
		// Vi skal først finde den inverse af H:
		qrdec(Q,R);
		qrinv(Q,R,invH);

		// Vi laver steppet delta x:
		gsl_blas_dgemv(CblasNoTrans,-1.0,invH,df,0.0,delta_x);

    double lambda = 1;
    gsl_vector* s = delta_x;
    double sdf;
    while(1){
	for(int i=0;i<n;i++){
	double si = gsl_vector_get(s,i);
	gsl_vector_set(x_step,i,si);
	}
	gsl_blas_daxpy(1.0,x,x_step);   //Vi laver den k+i'te x ved at lægge skridtet til det k'te x
	gsl_blas_ddot(s,df,&sdf);  // s_dot_df <- s*df



      if (f(x_step,df,H) < f(x,df,H) + alpha*sdf || lambda < lambda_min) {
        break;
      }
	//Hvis skridtet ikke godtages skalerer vi skridtstørrelsen
      lambda *= 0.5;
      gsl_blas_dscal(0.5,s);
    }

    gsl_blas_daxpy(1.0,s,x);
    steps++;
  } while (gsl_blas_dnrm2(df) > eps);

	gsl_vector_free(x_step);
	gsl_vector_free(df);
	gsl_vector_free(delta_x);
	gsl_matrix_free(H);
	gsl_matrix_free(Q);
	gsl_matrix_free(R);
	gsl_matrix_free(invH);

  return steps;
}

