#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include<stdio.h>

int newton (
        double f(gsl_vector* x,gsl_vector* df, gsl_matrix* H),
        gsl_vector* xstart, double eps);

void vector_print(char* s,gsl_vector* v){
        printf("%s",s);
        for(int i=0;i<v->size;i++)printf("%10.4g ",gsl_vector_get(v,i));
        printf("\n");
        }

//int qnewton(double cost(gsl_vector*x), gsl_vector*x, double eps);
void numeric_gradient(double cost(gsl_vector*x),gsl_vector*x, gsl_vector*g);

double rosen(gsl_vector*v, gsl_vector* df, gsl_matrix* H){
	double x=gsl_vector_get(v,0);
	double y=gsl_vector_get(v,1);
	double dfdx = -2*(1-x)-400*(y-x*x)*x;
	double dfdy = 200*(y-x*x);
	gsl_vector_set(df,0,dfdx);
	gsl_vector_set(df,1,dfdy);
	double d2fdx = -400*(y-x*x)+800*x*x+2;
	double dfdxy = -400*x;
	double d2fdy = 200;
	gsl_matrix_set(H,0,0,d2fdx); gsl_matrix_set(H,0,1,dfdxy);
	gsl_matrix_set(H,1,0,dfdxy); gsl_matrix_set(H,1,1,d2fdy);
	return pow(1-x,2)+100*pow(y-x*x,2);
}

double himmel(gsl_vector*v, gsl_vector* df, gsl_matrix* H){
	double x=gsl_vector_get(v,0);
	double y=gsl_vector_get(v,1);
        double dfdx = 4*x*(x*x+y-11)+2*(x+y*y-7);
        double dfdy = 2*(x*x+y-11)+4*y*(x+y*y-7);
        gsl_vector_set(df,0,dfdx);
        gsl_vector_set(df,1,dfdy);
        double d2fdx = 4*(x*x+y-11)+8*x*x+2;
        double dfdxy = 4*(x+y);
        double d2fdy = 4*(x+y*y-7)+8*y*y+2;
        gsl_matrix_set(H,0,0,d2fdx); gsl_matrix_set(H,0,1,dfdxy);
        gsl_matrix_set(H,1,0,dfdxy); gsl_matrix_set(H,1,1,d2fdy);
	return pow(x*x+y-11,2)+pow(x+y*y-7,2);
}

int main(){
	int n=2,nsteps;
	gsl_vector* x=gsl_vector_alloc(n); // Vi skriver vores startgæt i en vektor
	gsl_vector* df=gsl_vector_alloc(n);
	gsl_matrix* H=gsl_matrix_alloc(n,n);
//	gsl_vector* g=gsl_vector_alloc(n);
	double eps=1e-3;	// Vi laver en præcision
	printf("eps=%.1e\n",eps);


        printf("\nMINIMIZATION OF ROSENBROCK'S FUNCTION\n");
        printf("start point:\n");       // Print startgættet til vores funktion
        gsl_vector_set(x,0,-2);
        gsl_vector_set(x,1,8);
        gsl_vector_fprintf(stdout,x,"%g");
        printf("value at startpoint: %g\n",rosen(x,df,H));
        nsteps=newton(rosen,x,eps);
        printf("steps=%i\n",nsteps);
        printf("minimum found at:\n");  // I newton finder vi et minimum, og det smider vi så tilbage i x, og printer det.
        gsl_vector_fprintf(stdout,x,"%g");
        printf("value af found minimum: %g\n",rosen(x,df,H));    // Print funktionsværdien i det minimumspunkt vi har fundet.

	printf("\nMINIMIZATION OF HIMMELBLAU'S FUNCTION\n");
	printf("start point:\n");
	gsl_vector_set(x,0,7);
	gsl_vector_set(x,1,12);
	gsl_vector_set_zero(df);
	gsl_matrix_set_zero(H);
	gsl_vector_fprintf(stdout,x,"%g");
	printf("value at startpoint: %g\n",himmel(x,df,H));
	nsteps=newton(himmel,x,eps);
	printf("steps=%i\n",nsteps);
	printf("minimum found at:\n");
	gsl_vector_fprintf(stdout,x,"%g");
	printf("value at found minimum: %g\n",himmel(x,df,H));

return 0;
}
