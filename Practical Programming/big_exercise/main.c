#include<stdio.h>
#include<math.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_odeiv2.h>

int my_cos ( double x, const double y[], double dydx[], void *params) {
        dydx[0] = y[1];
        dydx[1] =-y[0];
  return GSL_SUCCESS;
}

double myode(double x){
        gsl_odeiv2_system sys;
        sys.function=my_cos;
        sys.jacobian=NULL;
        sys.dimension=2;
        sys.params=NULL;
        double hstart=copysign(0.1,x);
        double acc=1e-6, eps=1e-6;
        gsl_odeiv2_driver* driver =
                gsl_odeiv2_driver_alloc_y_new
                        (&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);

        double t=0;
        double y[2]={1.};
        gsl_odeiv2_driver_apply(driver,&t,x,y);

        gsl_odeiv2_driver_free(driver);
        return y[0];
}


int main () {
        for(double x=-1;x<3.5*M_PI;x+=0.1){
		if(x<0) printf("%g %g %g\n",x,myode(x+2*M_PI),cos(x+2*M_PI));
		if(x<2*M_PI) printf("%g %g %g\n",x,myode(x),cos(x));
		if(x>2*M_PI) printf("%g %g %g\n",x,myode(x-2*M_PI),cos(x-2*M_PI));
		}
        printf("\n\n");

return EXIT_SUCCESS;
        }

