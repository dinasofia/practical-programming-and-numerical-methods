#include<stdio.h>
#include<math.h>
#include<gsl/gsl_sf_airy.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_linalg.h>
#include<gsl/gsl_blas.h>

int main(){
	double y, acc = 1e-3;
	for(y = -11; y<2; y+=0.01){
		double ai = gsl_sf_airy_Ai(y,acc);
		double bi = gsl_sf_airy_Bi(y,acc);
		printf("%g %g %g\n",y,ai,bi);
		}

	int n = 3;
	gsl_matrix *A = gsl_matrix_alloc(n,n);
	gsl_matrix_set(A,0,0,6.13); gsl_matrix_set(A,0,1,-2.90); gsl_matrix_set(A,0,2,5.86);
	gsl_matrix_set(A,1,0,8.08); gsl_matrix_set(A,1,1,-6.31); gsl_matrix_set(A,1,2,-3.89);
	gsl_matrix_set(A,2,0,-4.36); gsl_matrix_set(A,2,1,1.00); gsl_matrix_set(A,2,2,0.19);

	gsl_vector *b = gsl_vector_alloc(n);
	gsl_vector_set(b,0,6.23); gsl_vector_set(b,1,5.37); gsl_vector_set(b,2,2.29);
	/*HH-solveren ødelægger A matricen i løsningsprocessen, så vi kopierer lige vores matrix
	over i A2 for at kunne tjekke den baegefter.*/
	gsl_matrix *A2 = gsl_matrix_alloc(n,n);
	gsl_matrix_memcpy(A2,A);
	gsl_vector *x = gsl_vector_alloc(n);
	gsl_vector *b2 = gsl_vector_alloc(n);

	gsl_linalg_HH_solve(A,b,x);
	gsl_blas_dgemv(CblasNoTrans,1.0,A2,x,0.0,b2);

	fprintf(stderr,"Vektor b:\n");
	for(int i=0;i<3;i++){
		fprintf(stderr,"%g\n",gsl_vector_get(b,i));
	}
        fprintf(stderr,"\nA*x:\n");
        for(int i=0;i<3;i++){
                fprintf(stderr,"%g\n",gsl_vector_get(b2,i));
        }

gsl_matrix_free(A);
gsl_matrix_free(A2);
gsl_vector_free(b);
gsl_vector_free(x);
gsl_vector_free(b2);
return 0;
}
