#include "stdio.h"
#include "math.h"
#include <complex.h>
int main(){
	printf("Beregn div. funktioner ved brug af math:\n\n");
	//Beregn gammafunktionen af 5
	double y = tgamma(5.0);
        printf("gamma(5) = %g\n",y);

	// Beregn Besselfunktionen af 0.5
	double x=j1(0.5);
	printf("bessel(0.5) = %g\n",x);

	complex p = cpow(-2.,0.5);
	printf("sqrt(-2) = %g + i%g\n",creal(p),cimag(p));

	complex z = cexp(I);
        printf("exp(i)=%g+i%g\n",creal(z),cimag(z));

	complex v = cexp(I*M_PI);
        printf("exp(i*pi) = %g+i%g\n",creal(v),cimag(v));

	complex w = cpow(I,M_E);
        printf("i^e=%g+i%g\n",creal(w),cimag(w));

	printf("\nTest af forskellige tal-typer:\n\n");


        float f = 0.11111111111111111111111111111111111;
        double d = 0.11111111111111111111111111111111111;
        long double ld = 0.11111111111111111111111111111111111L;
        printf("      float = %.25g\n     double = %.25lg\nlong double = %.25Lg\n",f,d,ld);

}
