#include <stdio.h>
#include <limits.h>
#include <float.h>
#include <math.h>
int equal(double a, double b, double tau, double epsilon);
int main(){
printf("--------------Exercise 3.1.i--------------\n");
	int j = INT_MAX;
	printf("INT_MAX = %i\n",j);
	int i = 1; while(i+1>i) {i++;}
	printf("my max int(while) = %i\n",i);
	int k;
	for (k = 1; k+1>k;k++){k;}
	printf("my max int(for)=%i\n",k);
	int l = 1;
	do{l++;}while(l+1>l);
	printf("my max int(do while) = %i\n",l);

printf("--------------Exercise 3.1.ii--------------\n");
	int m = INT_MIN;
	printf("INT_MIN = %i\n",m);
	int n = 1; while(n-1<n){n--;}
	printf("my min int(while)=%i\n",n);
	int o;
	for(o=1;o-1<o;o--){o;}
	printf("my min int(for)=%i\n",o);
	int p=1;
	do{p--;}while(p-1<p);
	printf("my min int(do while) = %i\n",p);

printf("--------------Exercise 3.1.iii--------------\n");
	float x = FLT_EPSILON;
	float y=1; while(1+y!=1){y/=2;} y*=2;
	float z; for(z=1; 1+z!=1; z/=2){} z*=2;
	float v=1; do{v/=2;}while(1+v!=1); v*=2;
	printf(" flt_eps = %g,\n flt_eps(while)=%g,\n flt_eps(for)=%g,\n flt_eps(do while)=%g\n",x,y,z,v);

	double w = DBL_EPSILON;
	double a=1; while(1+a!=1){a/=2;} a*=2;
	double b; for(b=1; 1+b!=1; b/=2){} b*=2;
	double c=1; do{c/=2;}while(1+c!=1); c*=2;
        printf(" dbl_eps = %g,\n dbl_eps(while)=%g,\n dbl_eps(for)=%g,\n dbl_eps(do while)=%g\n",w,a,b,c);

	long double d = LDBL_EPSILON;
	long double e=1; while(1+e!=1){e/=2;} e*=2;
	long double f; for(f=1; 1+f!=1; f/=2){} f*=2;
	long double g=1; do{g/=2;}while(1+g!=1); g*=2;
	printf(" ldbl_eps = %Lg,\n ldbl_eps(while)=%Lg,\n ldbl_eps(for)=%Lg,\n ldbl_eps(do while)=%Lg\n",d,e,f,g);

printf("\n------------- Exercise 3.2.i og ii ----------------\n");
        int max = INT_MAX/3;
        int aa=1;
        float bb = 0.0;
        while(aa<=max){aa++,bb=bb+1.0f/aa;}
        printf("sum_up_float = %g\n",bb);

        int cc = 0;
        float dd=0.0;
        while(cc<max-1){cc++,dd=dd+1.0f/(max-cc);}
        printf("sum_down_float = %g\n\n",dd);

	printf("Forskellen i de to summer kommer af hvordan tallene bliver \nrundet af inden de bliver langt sammen.");

printf("\n------------- Exercise 3.2.iv ----------------\n");

        int ee=1;
        double ff = 0.0;
        while(ee<=max){ee++,ff=ff+1.0d/ee;}
        printf("sum_up_double = %g\n",ff);

        int gg = 0;
        double hh=0.0;
        while(gg<max-1){gg++,hh=hh+1.0d/(max-gg);}
        printf("sum_down_double = %g\n",hh);

	printf("Her er forskellen meget mindre, fordi doubles har mangle flere\ndecimaler end floats, og derfor gør afrundingen ikke lige \nså stor forskel\n");
        equal(4,2,7,6);

}
