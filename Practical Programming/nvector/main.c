#include "nvector.h"
#include "stdio.h"
#include "stdlib.h"
#define RND (double)rand()/RAND_MAX

int main()
{
	int n = 5;

	printf("\nTesting nvector_alloc:\n");
	nvector *v = nvector_alloc(n);
	if (v == NULL) printf("test failed\n");
	else printf("test passed\n");

	printf("\nTesting nvector_set and nvector_get:\n");
	double value = RND;
	int i = n / 2;
	nvector_set(v, i, value);
      	double vi = nvector_get(v, i);
	if (vi== value) printf("test passed\n");
	else printf("test failed\n");

	nvector *a = nvector_alloc(n);
	nvector *b = nvector_alloc(n);

	int j;
	double dot_should = 0;
	for(j=0; j<n; j++){
	double aj=RND, bj=RND, dotj = aj*bj;
	nvector_set(a,j,aj);
        nvector_set(b,j,bj);
	dot_should += dotj;
        }

	printf("\n\n-----Test af prikprodukt-------\n");
	double dot = nvector_dot_product(a,b);
	printf("Dot product, should = %g\n",dot_should);
	printf("Dot product, actually = %g\n",dot);

	nvector_free(v);
	nvector_free(a);
	nvector_free(b);
	return 0;
}
