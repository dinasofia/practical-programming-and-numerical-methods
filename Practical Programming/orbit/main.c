#include<stdio.h>
#include<unistd.h> /* short comman-line options */
#include<getopt.h> /* long and short comman-line options */
#include<math.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_odeiv2.h>

int orbital_1 ( double x, const double y[], double dydx[], void *params) {
        dydx[0] = y[1];
        dydx[0] = y[0]*(1-y[0]);
  return GSL_SUCCESS;
}

double myode(double x){
        gsl_odeiv2_system sys;
        sys.function=orbital_1;
        sys.jacobian=NULL;
        sys.dimension=2;
        sys.params=NULL;
	double hstart=copysign(0.1,x);
        double acc=1e-6, eps=1e-6;
        gsl_odeiv2_driver* driver =
                gsl_odeiv2_driver_alloc_y_new
                        (&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);

        double t=0;
        double y[2]={0.5};
        gsl_odeiv2_driver_apply(driver,&t,x,y);

        gsl_odeiv2_driver_free(driver);
        return y[0];
}


int orbital_equation (double phi, const double y[], double yprime[], void *params) {
  double epsilon = *(double *) params;
  yprime[0] = y[1];
  yprime[1] = 1 - y[0] + epsilon * y[0] * y[0];
  return GSL_SUCCESS;
}

int main (int argc, char **argv) {
        for(double x=0;x<3;x+=0.1)
                printf("%g %g\n",x,myode(x));
        printf("\n\n");
        for(double x=0;x<3;x+=0.1)
                printf("%g %g\n",x,1/(1+exp(-x)));

	printf("\n\n");

	double epsilon_vek[] = {0,0,0.01}, uprime_vek[] = {0,-0.5,-0.5};
/*
	while(1){
		int opt = getopt(argc, argv, "e:p:");
		if(opt == -1) break;
		switch(opt){
			case 'e': epsilon=atof(optarg); break;
			case 'p': uprime=atof(optarg); break;
			default:
                   fprintf(stderr, "Usage: %s [-e epsilon] [-p uprime]\n", argv[0]);
                   exit(EXIT_FAILURE);
		}
	}*/

	for(int i=0;i<3;i++){
	double epsilon = epsilon_vek[i], uprime = uprime_vek[i];
	gsl_odeiv2_system orbit;
	orbit.function = orbital_equation;
	orbit.jacobian = NULL;
	orbit.dimension = 2;
	orbit.params = (void *) &epsilon;

	double hstart = 1e-3, epsabs = 1e-6, epsrel = 1e-6;
	double phi_max =39.5 * M_PI, delta_phi = 0.05;

	gsl_odeiv2_driver *driver =
		gsl_odeiv2_driver_alloc_y_new
			(&orbit, gsl_odeiv2_step_rk8pd, hstart, epsabs, epsrel);

	double t = 0, y[2] = { 1, uprime };
	for (double phi = 0; phi < phi_max; phi += delta_phi) {
		int status = gsl_odeiv2_driver_apply (driver, &t, phi, y);
		printf ("%g %g\n", phi, y[0]);
		if (status != GSL_SUCCESS) fprintf (stderr, "fun: status=%i", status);
		}
	printf("\n\n");
	gsl_odeiv2_driver_free (driver);
	}

return EXIT_SUCCESS;
	}

