#include<math.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>

double ham_integrand(double x, void* params){
	double z = *(double*)params;
	return (-pow(z,2)*pow(x,2)/2 + z/2 + pow(x,2))*exp(-z*pow(x,2));
}

double hamilton(double z){

	gsl_function f;
	f.function = ham_integrand;
	f.params = (void*)&z;

	int limit = 100;
	double acc=1e-6,eps=1e-6,result,err;
	gsl_integration_workspace * workspace =
		gsl_integration_workspace_alloc(limit);
	gsl_integration_qagi(&f,acc,eps,limit,workspace,&result,&err);
		gsl_integration_workspace_free(workspace);
	return result;
}
