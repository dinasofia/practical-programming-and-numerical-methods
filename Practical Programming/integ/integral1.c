#include<stdio.h>
#include<math.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>

double integrand1(double x, void* params){
	return log(x)/sqrt(x);
}

double log_function(){

	gsl_function f;
	f.function = integrand1;
	f.params = NULL;

	int limit = 100;
	double a=0, b = 1.0,acc=1e-6,eps=1e-6,result,err;
	gsl_integration_workspace * workspace =
		gsl_integration_workspace_alloc(limit);
	gsl_integration_qags(&f,a,b,acc,eps,limit,workspace,&result,&err);
		gsl_integration_workspace_free(workspace);
	return result;
}

