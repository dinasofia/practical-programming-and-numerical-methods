#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int diff_equation
(double x, const double y[], double dydx[], void * params)
{
	dydx[0]=y[1];
	dydx[0]=(2/sqrt(M_PI))*exp(-x*x);
return GSL_SUCCESS;
}

double err_func(double x){
	gsl_odeiv2_system sys;
	sys.function = diff_equation;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = NULL;

	double acc=1e-6,eps=1e-6,hstart=copysign(0.1,x);
	gsl_odeiv2_driver *driver=gsl_odeiv2_driver_alloc_y_new
		(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);


	double t=0,y[2]={0,2/sqrt(M_PI)};
	gsl_odeiv2_driver_apply(driver,&t,x,y);


	gsl_odeiv2_driver_free(driver);
	return y[0];
}

int main(int argc, char** argv){
	double a=-5;
	if(argc>1){
	a=atof(argv[1]);}

	double b = 5;
	if(argc>2){
	b=atof(argv[2]);}

	double dx = 0.2;
	if(argc>3){
	dx = atof(argv[3]);}

	for(double x=a;x<b+dx;x+=dx)
		printf("%g %g\n",x,err_func(x));
return 0;
}

