#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define RND ((double)rand()/RAND_MAX)
#define FMT "%7.3f"

double jacobian(gsl_matrix *A, gsl_matrix *V);
void printm(gsl_matrix *A){
        for(int r=0;r<A->size1;r++){
                for(int c=0;c<A->size2;c++) printf(FMT,gsl_matrix_get(A,r,c));
                printf("\n");}
printf("\n\n");
}
void vector_print(gsl_vector *v){
        for(int i=0;i<v->size;i++) printf(FMT,gsl_vector_get(v,i));
        printf("\n");
        }

int main(){
	int n = 6;
	gsl_matrix *N = gsl_matrix_calloc(n,n);
	gsl_matrix *I = gsl_matrix_calloc(n,n);
	gsl_matrix *A = gsl_matrix_calloc(n,n);
	gsl_matrix *V = gsl_matrix_calloc(n,n);
	gsl_matrix *Nog = gsl_matrix_calloc(n,n);
	gsl_matrix *sqrtinvD = gsl_matrix_calloc(n,n);
	gsl_matrix *sqrtD = gsl_matrix_calloc(n,n);
	gsl_matrix *m1 = gsl_matrix_calloc(n,n);
	gsl_matrix *m2 = gsl_matrix_calloc(n,n);
	gsl_matrix *m3 = gsl_matrix_calloc(n,n);
	gsl_matrix *B = gsl_matrix_calloc(n,n);
	gsl_matrix *Vb = gsl_matrix_calloc(n,n);
	gsl_matrix *X = gsl_matrix_calloc(n,n);
	gsl_vector *x = gsl_vector_calloc(n);
        gsl_vector *y1 = gsl_vector_calloc(n);
        gsl_vector *y2 = gsl_vector_calloc(n);

	gsl_matrix_set_identity(I);
printf("---------------------- Eksamensopgave -----------------");
printf("Jeg har løst opgaven og det generaliserede egenværdi problem.\n");
printf("Denne er ikke på listen over de 18 eksamensopgaver, men det var ikke\n");
printf("klart for os at det var en af dem man skulle tage da vi lavede opgaven.\n\n");

printf("\nVi ønsker at løse det generaliserede egenværdi problem:\n Ax = lambda*Nx \nHvor A er en symmetrisk matrix, x er de generaliserede egenvektorer for A,\nlambda er de tilhørende generaliserede egenværdier og \nN er en symmetrisk matrix med positive egenværdier.\n\n");

// Lav a der skal være en reel symmetrisk matrix:
	for(int i=0;i<n;i++)for(int j=i;j<n;j++){
        	gsl_matrix_set(A,i,j,RND);
	        double aij = gsl_matrix_get(A,i,j);
        	gsl_matrix_set(A,j,i,aij);
                gsl_matrix_set(N,i,j,RND);
                double nij = gsl_matrix_get(N,i,j);
                gsl_matrix_set(N,j,i,nij);
}
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,n,I,I,1,N);

printf("Tilfældig symmetrisk matrix A:\n");
printm(A);
printf("Symmetrisk matrix med posisitve egenværdier N:\n");
printm(N);
printf("Der sikres at N har positive egenværdier ved at konstruere først en \n");
printf("tilfældig symmetrisk matrix af dimension n, og derefter lægge n*I til.\n\n");
printf("Først laves dekomposition af N\n N = (V^T)DV \n");
printf("Hvor D er en diagonal-matrix med egenværdierne for N på diagonalen. \n");
printf("Og V har egenvektorerne for N som søjler.\n");

gsl_matrix_set_identity(V);
gsl_matrix_memcpy(Nog,N);

jacobian(N,V);
printf("Matrix med eg.værdier for N, D:\n");
printm(N);
printf("Matrix med eg.vektorer for N, V:\n");
printm(V);

printf("Vi kan nu løse det generaliserede egenværdi problem ved at løse:\n By = lambda*y\n");
printf("Hvor: B = sqrt(D^-1)*(V^T)*A*V*sqrt(D^-1), og y = sqrt(D)*(V^T)\n\n");
printf("Da D er en diagonal matrix er sqrt(D^-1) bare en diagonal matrix der har som\n");
printf("diagonalelementer kvadratroden af den inverse af diagonalelementerne i D\n\n");
printf("Vi ser at det er det samme at løse de to problemer ved:\n");
printf("Ax = lam*N*x = lam*V*D*(V^T)*x = lam*V*sqrt(D)*sqrt(D)*(V^T)*x = lam*V*sqrt(D)*y\n\n");
printf("Vi bruger at pr. konstruktion: V^-1 = V^T\n");
printf("=> sqrt(D^-1)*(V^T) = lam*y\n\n");
printf("Vi indsætter produktet: V*sqrt(D^-1)*sqrt(D)*V^T = I\n");
printf("=> sqrt(D^-1)*(V^T)*A*V*sqrt(D^-1)*sqrt(D)*(V^T)*x = lam*y\n");
printf("=> B*y = lam*y\nsom ønsket\n\n");

for(int i =0;i<n;i++){
	double get_val = gsl_matrix_get(N,i,i);
	gsl_matrix_set(sqrtinvD,i,i,pow(get_val,-0.5));
	gsl_matrix_set(sqrtD,i,i,pow(get_val,0.5));
	}

// Her laver vi så den der B-matrix
gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0,sqrtinvD,V,0.0,m1);
gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0,m1,A,0.0,m2);
gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0,m2,V,0.0,m3);
gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0,m3,sqrtinvD,0.0,B);

printf("Matricen B konstrueres ud fra definitionen:\n");
printm(B);

gsl_matrix_set_identity(Vb);
jacobian(B,Vb);
printf("Herefter laves dekomposition af B:\n B = (W^T)*C*W \nhvor W og C har samme egenskaber som hhv. V og D i dekom. af N.\n\n");
printf("Matrix C, med eg.værdier af B, som også er eg.værdier for A:\n");
printm(B);
printf("Matrix W, der indeholder y-vektorerne, der er eg.vektorer for B:\n");
printm(Vb);

//Her laver vi matricen X, der som søjler har vektorerne x, som er dem vi leder efter:
gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,m1,Vb,0.0,X);

printf("De generaliserede eg.vektorer, x, for A, kan nu beregnes ud fra definitionen af y.\n");
printf("Matrix X, der som søjler har vektorerne x:\n");
printm(X);

printf("Herefter kontrolleres det at de generaliserede egenværdier er de rigtige, ved at \n");
printf("beregne for hver x og tilhørende lambda, A*x og lam*N*x og se at vi får det samme:\n\n");
for(int i=0;i<n;i++){
	gsl_vector_set_zero(y1);
	gsl_vector_set_zero(y2);
	gsl_matrix_get_col(x,X,i);
	gsl_blas_dgemv(CblasNoTrans,1.0,A,x,0.0,y1);
	double eigval = gsl_matrix_get(B,i,i);
	gsl_blas_dgemv(CblasNoTrans,eigval,Nog,x,0.0,y2);

	printf("\nA*x_(%d):\n",i);
	vector_print(y1);
	printf("lambda_(%d)*N*x_(%d):\n",i,i);
	vector_print(y2);
}

gsl_vector_free(x);
gsl_vector_free(y1);
gsl_vector_free(y2);
gsl_matrix_free(A);
gsl_matrix_free(B);
gsl_matrix_free(X);
gsl_matrix_free(N);
gsl_matrix_free(V);
gsl_matrix_free(Vb);
gsl_matrix_free(m1);
gsl_matrix_free(m2);
gsl_matrix_free(m3);
gsl_matrix_free(Nog);
gsl_matrix_free(sqrtinvD);
gsl_matrix_free(sqrtD);
gsl_matrix_free(I);
}
