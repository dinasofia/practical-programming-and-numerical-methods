---------------------- Eksamensopgave -----------------Jeg har løst opgaven og det generaliserede egenværdi problem.
Denne er ikke på listen over de 18 eksamensopgaver, men det var ikke
klart for os at det var en af dem man skulle tage da vi lavede opgaven.


Vi ønsker at løse det generaliserede egenværdi problem:
 Ax = lambda*Nx 
Hvor A er en symmetrisk matrix, x er de generaliserede egenvektorer for A,
lambda er de tilhørende generaliserede egenværdier og 
N er en symmetrisk matrix med positive egenværdier.

Tilfældig symmetrisk matrix A:
  0.840  0.783  0.912  0.335  0.278  0.477
  0.783  0.365  0.952  0.636  0.142  0.016
  0.912  0.952  0.137  0.157  0.130  0.999
  0.335  0.636  0.157  0.513  0.613  0.638
  0.278  0.142  0.130  0.613  0.494  0.293
  0.477  0.016  0.999  0.638  0.293  0.527


Symmetrisk matrix med posisitve egenværdier N:
  6.394  0.798  0.198  0.768  0.554  0.629
  0.798  6.513  0.916  0.717  0.607  0.243
  0.198  0.916  6.804  0.401  0.109  0.218
  0.768  0.717  0.401  6.839  0.296  0.524
  0.554  0.607  0.109  0.296  6.973  0.771
  0.629  0.243  0.218  0.524  0.771  6.770


Der sikres at N har positive egenværdier ved at konstruere først en 
tilfældig symmetrisk matrix af dimension n, og derefter lægge n*I til.

Først laves dekomposition af N
 N = (V^T)DV 
Hvor D er en diagonal-matrix med egenværdierne for N på diagonalen. 
Og V har egenvektorerne for N som søjler.
Matrix med eg.værdier for N, D:
  5.338  0.000 -0.000  0.000  0.000 -0.000
  0.000  5.799  0.000  0.000  0.000  0.000
 -0.000  0.000  6.185 -0.000 -0.000 -0.000
  0.000  0.000 -0.000  6.520 -0.000  0.000
  0.000  0.000 -0.000 -0.000  7.114  0.000
 -0.000  0.000 -0.000  0.000  0.000  9.338


Matrix med eg.vektorer for N, V:
  0.494  0.668  0.225 -0.274 -0.082  0.420
 -0.702  0.231  0.329  0.166  0.331  0.456
  0.389 -0.101 -0.393  0.431  0.630  0.319
  0.037 -0.578  0.054 -0.666  0.152  0.441
  0.189 -0.370  0.354  0.515 -0.516  0.412
 -0.273  0.139 -0.747 -0.041 -0.443  0.387


Vi kan nu løse det generaliserede egenværdi problem ved at løse:
 By = lambda*y
Hvor: B = sqrt(D^-1)*(V^T)*A*V*sqrt(D^-1), og y = sqrt(D)*(V^T)

Da D er en diagonal matrix er sqrt(D^-1) bare en diagonal matrix der har som
diagonalelementer kvadratroden af den inverse af diagonalelementerne i D

Vi ser at det er det samme at løse de to problemer ved:
Ax = lam*N*x = lam*V*D*(V^T)*x = lam*V*sqrt(D)*sqrt(D)*(V^T)*x = lam*V*sqrt(D)*y

Vi bruger at pr. konstruktion: V^-1 = V^T
=> sqrt(D^-1)*(V^T) = lam*y

Vi indsætter produktet: V*sqrt(D^-1)*sqrt(D)*V^T = I
=> sqrt(D^-1)*(V^T)*A*V*sqrt(D^-1)*sqrt(D)*(V^T)*x = lam*y
=> B*y = lam*y
som ønsket

Matricen B konstrueres ud fra definitionen:
 -0.113  0.081 -0.001  0.015 -0.070  0.047
  0.081  0.060 -0.028  0.026  0.113  0.034
 -0.001 -0.028  0.076 -0.001  0.012 -0.043
  0.015  0.026 -0.001 -0.034 -0.005 -0.006
 -0.070  0.113  0.012 -0.005  0.001  0.047
  0.047  0.034 -0.043 -0.006  0.047  0.307


Herefter laves dekomposition af B:
 B = (W^T)*C*W 
hvor W og C har samme egenskaber som hhv. V og D i dekom. af N.

Matrix C, med eg.værdier af B, som også er eg.værdier for A:
 -0.212 -0.000  0.000 -0.000 -0.000 -0.000
 -0.000 -0.047  0.000 -0.000 -0.000  0.000
  0.000  0.000  0.011  0.000  0.000 -0.000
 -0.000 -0.000 -0.000  0.074 -0.000  0.000
 -0.000 -0.000  0.000 -0.000  0.133  0.000
 -0.000  0.000 -0.000  0.000  0.000  0.339


Matrix W, der indeholder y-vektorerne, der er eg.vektorer for B:
  0.745  0.295  0.578  0.092  0.054  0.112
 -0.427  0.247  0.295  0.056  0.781  0.238
 -0.073  0.056  0.253 -0.947 -0.034 -0.171
  0.011 -0.896  0.422  0.054  0.126  0.004
  0.499 -0.208 -0.580 -0.258  0.521  0.181
 -0.091 -0.044  0.012 -0.149 -0.314  0.932


De generaliserede eg.vektorer, x, for A, kan nu beregnes ud fra definitionen af y.
Matrix X, der som søjler har vektorerne x:
  0.005  0.233  0.203 -0.069  0.152  0.197
 -0.228 -0.149 -0.157 -0.199  0.080  0.128
  0.265 -0.175 -0.020  0.095  0.093  0.177
  0.125  0.162 -0.197 -0.083 -0.236  0.085
  0.010 -0.152  0.237 -0.095 -0.238  0.039
 -0.185  0.006 -0.036  0.300 -0.080  0.140


Herefter kontrolleres det at de generaliserede egenværdier er de rigtige, ved at 
beregne for hver x og tilhørende lambda, A*x og lam*N*x og se at vi får det samme:


A*x_(0):
  0.024  0.251 -0.340 -0.150  0.031  0.249
lambda_(0)*N*x_(0):
  0.024  0.251 -0.340 -0.150  0.031  0.249

A*x_(1):
 -0.065  0.043  0.058 -0.051  0.047 -0.004
lambda_(1)*N*x_(1):
 -0.065  0.043  0.058 -0.051  0.047 -0.004

A*x_(2):
  0.012 -0.010 -0.003 -0.014  0.017 -0.001
lambda_(2)*N*x_(2):
  0.012 -0.010 -0.003 -0.014  0.017 -0.001

A*x_(3):
 -0.038 -0.097  0.035 -0.044 -0.045  0.137
lambda_(3)*N*x_(3):
 -0.038 -0.097  0.035 -0.044 -0.045  0.137

A*x_(4):
  0.092  0.052  0.080 -0.201 -0.220 -0.095
lambda_(4)*N*x_(4):
  0.092  0.052  0.080 -0.201 -0.220 -0.095

A*x_(5):
  0.533  0.431  0.483  0.332  0.208  0.412
lambda_(5)*N*x_(5):
  0.533  0.431  0.483  0.332  0.208  0.412
